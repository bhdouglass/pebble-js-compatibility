var Pebble = function() {

};

Pebble.prototype.addEventListener = function(type, fn) {
    if (type == 'ready') {
        setTimeout(function() {
            fn(new Event('ready'));
        });
    }
};

Pebble.prototype.sendAppMessage = function(data, ack, nack) {
    console.log('Pretending to send: ' + JSON.stringify(data));
    ack(new Event('message-ack'));
};

window.Pebble = new Pebble();
