#include <pebble.h>
#include "helpers.h"

static Window *window;
static TextLayer *text_layer;
static char text[30];

static void handle_outbox_failed(DictionaryIterator *iterator, AppMessageResult reason, void *context) {
    APP_LOG(APP_LOG_LEVEL_DEBUG, "message failed to send: %s", translate_message_error(reason));
}

static void handle_inbox_received(DictionaryIterator *iter, void *context) {
    int specs = 0;
    int failures = 0;

    Tuple *data = dict_read_first(iter);
    while(data != NULL) {
        switch(data->key) {
            case 0: //specs
                specs = data->value->int32;
                break;

            case 1: //failures
                failures = data->value->int32;
                break;
        }

        data = dict_read_next(iter);
    }

    if (failures > 0) {
        snprintf(text, sizeof(text), "%d failed out of %d tests", failures, specs);
    }
    else {
        snprintf(text, sizeof(text), "All %d tests passed", specs);
    }

    text_layer_set_text(text_layer, text);
    vibes_short_pulse();
}

static void handle_inbox_dropped(AppMessageResult reason, void *context) {
    APP_LOG(APP_LOG_LEVEL_DEBUG, "message dropped: %s", translate_message_error(reason));
}

static void handle_outbox_sent(DictionaryIterator *iterator, void *context) {
    APP_LOG(APP_LOG_LEVEL_DEBUG, "message sent sucessfully");
}

static void window_load(Window *window) {
    Layer *window_layer = window_get_root_layer(window);
    GRect window_bounds = layer_get_bounds(window_layer);
    int PWIDTH = window_bounds.size.w;
    int PHEIGHT = window_bounds.size.h;

    text_layer = text_layer_init(
        window_layer,
        GRect(0, PHEIGHT / 2 - 20, PWIDTH, 30),
        fonts_get_system_font(FONT_KEY_GOTHIC_18),
        GColorClear,
        GColorBlack,
        GTextAlignmentCenter
    );

    strncpy(text, "Running tests", sizeof(text));
    text_layer_set_text(text_layer, text);
}

static void window_unload(Window *window) {
    text_layer_destroy(text_layer);
}

static void init(void) {
    window = window_create();
    window_set_window_handlers(window, (WindowHandlers) {
        .load = window_load,
        .unload = window_unload,
    });

    const bool animated = true;
    window_stack_push(window, animated);

    app_message_register_outbox_failed(&handle_outbox_failed);
    app_message_register_inbox_received(&handle_inbox_received);
    app_message_register_inbox_dropped(&handle_inbox_dropped);
    app_message_register_outbox_sent(&handle_outbox_sent);
    app_message_open(640, 64);
}

static void deinit(void) {
    window_destroy(window);
}

int main(void) {
    init();
    app_event_loop();
    deinit();
}
